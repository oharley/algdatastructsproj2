/* Min Indexed Binary Heap - Oliver Harley 575246 - October 2014
 * This implementation is array backed and the 0th element holds the length
 * of the array, (ignoring itself).
 * It will take only positive cost were the lower the cost the
 * higher the priority.
 *
 * This heap is NOT dynamically lengthened, it is up to the initializer to
 * correctly free it.*/

#include "binheap.h"

/* Initialises an array based heap where the first node contains it's length.
   The heap is created to be length 'sizeOfHeap'*/
indexd_heap makeHeap(HEAP_LEN_TYPE sizeOfHeap) {
    indexd_heap H;
    HEAP_LEN_TYPE i = 0;

    /* Check that we can actually store this sized heap*/
    if (MAX_HEAP_LEN < sizeOfHeap + 1) {
        debug(__LINE__, __FILE__, EXIT_FAILURE,
              "Trying to make heap too large!\nMax size allowed is: %d,"
              "requested %d", MAX_HEAP_LEN - 1, sizeOfHeap);
    }

    /* sizeOfHeap doesn't include the empty 0th element*/
    H.heap = malloc_s(sizeof(struct bin_heap_node) *
                      (sizeOfHeap + 1));
    H.idx2heap = malloc_s(sizeof(HEAP_LEN_TYPE)*(sizeOfHeap + 1));

    /* Zero data, and set cost to the max value (Lowest cost)*/
    for (i = 0; i < sizeOfHeap; ++i) {
        H.heap[i].data = NULL;
        H.heap[i].cost = GREATEST_COST;
        H.heap[i].data_size = sizeof(void*);
        H.idx2heap[i] = i;
    }

    H.length = 0;
    return H;
}

/* Structures the array so the lowest cost is bought up to the top of the heap.
 * Takes the pointer to the heap, the element (indexed from 1) from where to
 * heapify up from.*/
void downHeap(indexd_heap *H, HEAP_LEN_TYPE i) {
    /* To stop array out of bounds*/
    assert(0 != i);

    COST_TYPE l_cost, r_cost, min_cost;
    HEAP_LEN_TYPE min = i;

    l_cost = H->heap[2 * i].cost; r_cost = H->heap[2 * i + 1].cost;
    min_cost = H->heap[i].cost;

    if ((2 * i + 1 <= H->length) && (r_cost < min_cost))
        min = 2 * i + 1;
    else if ((2 * i <= H->length) && (l_cost < min_cost))
        min = 2 * i;

    if (i != min) {
        /* Update the index that points to the heap's location*/
        swap_nodes(H, i, min);
        downHeap(H, min);
    }
}

/* Structures the array so the lowest cost is at the top of the heap.
* Takes the pointer to the heap, the element (indexed from 1)from where to
* heapify down from.*/
void upHeap(indexd_heap *H, HEAP_LEN_TYPE i) {
    /* To stop array out of bounds*/
    assert(0 != i);
    assert(H->length >= i); /*TODO check the +1*/

    COST_TYPE par_cost, moving_cost;

    /* We have reached the top of the heap*/
    if (1 == i) {
        return;
    }

    moving_cost = H->heap[i].cost;
    par_cost = H->heap[i / 2].cost; /*Use integer division on purpose*/

    /* If the parent's cost is more than i's, swap them, and check above*/
    if (par_cost >= moving_cost) {
        swap_nodes(H, i / 2, i);
        upHeap(H, i / 2);
    } else {
        /* The node is in the right place*/
        return;
    }
}

/* Peeks the top node, assumes the heap is in order*/
struct bin_heap_node* getLeastCost(indexd_heap *H) {
    return &H->heap[1];
}

/* Swaps the nodes, and adjusts the index array accordingly*/
void swap_nodes(indexd_heap *H, HEAP_LEN_TYPE i, HEAP_LEN_TYPE j) {
    H->idx2heap[H->heap[i].idx] = j;
    H->idx2heap[H->heap[j].idx] = i;
    swap(&H->heap[i], &H->heap[j], sizeof(H->heap[i]));
}

/* Removes the top node, and returns a copy then makes the array satisfy
 * heap condition*/
struct bin_heap_node* deleteMin(indexd_heap *H) {
    assert(0 != H->length);

    /* Make a copy to return*/
    struct bin_heap_node* r = copyNode(&H->heap[1]);
    r->idx = H->heap[1].idx;

    /* Reorganize the heap, so min cost is at the top again*/
    swap_nodes(H, 1, H->length);
    H->heap[H->length].cost = MAX_HEAP_LEN;
    H->heap[H->length].idx = 0;
    --H->length;

    if (0 != H->length)
        downHeap(H, 1);
    return r;
}

/* Inserts a copy of the node at the end of the heap, and reorders*/
void insertCopy(indexd_heap *H, struct bin_heap_node n) {
    addToEnd(H, n);
    upHeap(H, H->length); /* Put it in the correct place*/
}

/* Adds a node at the end of the heap, does not reorder the heap*/
void addToEnd(indexd_heap *H, struct bin_heap_node n) {
    ++H->length;
    HEAP_LEN_TYPE tmp = H->length;

    /* Copy data from n to the end of the heap*/
    H->idx2heap[tmp] = n.idx;
    H->heap[tmp].cost = n.cost;

    /* Link the index array's entry to the heap node entry's index*/
    H->heap[tmp].idx = (H->idx2heap[tmp]);

    if (NULL != n.data) {
        H->heap[tmp].data = malloc_s(n.data_size);
        memmove(H->heap[tmp].data, n.data, sizeof(n.data));
    } else {
        H->heap[tmp].data_size = 0;
        H->heap[tmp].data = NULL;
    }
}

/* Creates a copy of a Node*/
struct bin_heap_node* copyNode(struct bin_heap_node *N) {
    struct bin_heap_node* r = (struct bin_heap_node*)malloc_s(sizeof(*N));
    memcpy(r, N, sizeof(*N));

    r->idx = N->idx;

    return r;
}

/* Frees all nodes, and the data pointers within the heap.
 * BEWARE if the data pointer needs additional freeing this should
 * be done first. or using the freeDataFunc parameter*/
void destroyHeap(indexd_heap *H, HEAP_LEN_TYPE len,
                 void(*freeDataFunc)(void*, size_t)) {
    HEAP_LEN_TYPE i;

    for (i = 1; i < len; i++) {
        if (NULL != H->heap[i].data) {
            freeDataFunc(H->heap[i].data, H->heap[i].data_size);
            free(H->heap[i].data);
        }
    }
    free(H->heap);
    free(H->idx2heap);
}


/* Updates the priority of a node, does not reorder*/
void updatePriority(indexd_heap *H, HEAP_LEN_TYPE idx, HEAP_LEN_TYPE newPri) {
    H->heap[H->idx2heap[idx]].cost = newPri;
}

/* Implement at a later date*/
/*void heapSort(indexd_heap *H) {
}*/
