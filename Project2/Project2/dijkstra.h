/* Oliver Harley - 575246 October 2014*/
#ifndef DIJKSTRA_H
#define DIJKSTRA_H
/* Change INDEX_TYPE to an unsigned integer type to change the maximum length/
 * costs that dijkstra can handle. Also change the corresponding heap file's
 * length type. Or use that as the best choice for this.*/
#define INDEX_TYPE unsigned int 

/* This type must not be able to hold more than the MAX_HEAP_LEN of binheap.h*/

typedef struct Graph{
    struct vertex_t *vertexArray;
    struct ll_head *adjancencyArray;
    INDEX_TYPE *predecessors;
    INDEX_TYPE length;
} Graph; 

#include "stds.h"
#include "binheap.h"
#include <limits.h>
#include "linkedlist.h"
#include "holiday.h" /* Change this for different vertex/edge definitions*/

/* Returns the Max value for an unsigned type t*/
#ifndef USIGNED_MAXOF
#define USIGNED_MAXOF(t) ((t)(~(t) 0))
#endif

#define MAX_INDEX (USIGNED_MAXOF(INDEX_TYPE))
#define UNDEF 0

INDEX_TYPE* dijkstra(Graph *g, vertex_t* conditions,
                     int(*matchPref)(Graph*, vertex_t*, INDEX_TYPE));
void addToQ(indexd_heap* Q, INDEX_TYPE idx, INDEX_TYPE cost);
void freeGraph(Graph* g, INDEX_TYPE n_vertices);
#endif
