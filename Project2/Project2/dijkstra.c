﻿/* Oliver Harley - 575246 October 2014*/
/* An implementation of Dijkstra's Algorithm*/

#include "dijkstra.h"
#include "holiday.h"
#define TRUE (1)
#define FALSE (0)

/* Adds to the end of the queue an item with index idx, and with a
 * priority of cost. Does not reorder the queue.*/
void addToQ(indexd_heap* Q, INDEX_TYPE idx, INDEX_TYPE cost) {
    struct bin_heap_node entry;
    entry.data = NULL;
    entry.data_size = sizeof(NULL);
    entry.cost = cost;
    entry.idx = idx;
    addToEnd(Q, entry);
}

/*Performs Dijkstra's algorithm on graph G and with a condition's vertex. 
 *If a matchPref function is specified every time we have visited a vertex.
 *Returns an array of aggregate costs where the 0th element contains the final
 *destination's index within the graph's vertex array*/
INDEX_TYPE* dijkstra(Graph *g, vertex_t* conditions,
                     int(*matchPref)(Graph*, vertex_t*, INDEX_TYPE)) {
    /* Initialise a bunch of stuff*/
    INDEX_TYPE n_vertices = g->length;
    INDEX_TYPE source_idx = conditions->node_index;
    INDEX_TYPE *distA = NULL, *vistedA = NULL, i, distanceTmp;
    indexd_heap Q = makeHeap(n_vertices);
    struct ll_node *adjListTmp;
    struct bin_heap_node* min;
    vertex_t *v;
    edge_t u;


    /* The 0th element is used as an undefined/sentinel vertex index*/
    distA = malloc_s(sizeof(INDEX_TYPE)*(n_vertices + 1));
    vistedA = malloc_s(sizeof(INDEX_TYPE)*(n_vertices + 1));
    /* Assumes that the graph does not have a predecessor array already, and 
     * set to 0/UNDEF*/
    g->predecessors = calloc_s(n_vertices + 1, sizeof(INDEX_TYPE));

    /* Initialise the arrays*/
    distA[source_idx] = 0;
    for (i = 1; i <= n_vertices; i++) {
        if (i != source_idx) {
            distA[i] = MAX_INDEX;
        }
        /* Initialise the queue with vertices to check. The queue holds vertex
         * indexes to the graph vertex array and costs.*/
        addToQ(&Q, g->vertexArray[i].node_index, distA[i]);

        /* Initialise nodes as unvisited*/
        vistedA[i] = FALSE;
    }
    vistedA[source_idx] = TRUE; /* the starting vertex begins as visited*/

    /* Bring the lowest cost (highest priority) item to the top of the queue*/
    upHeap(&Q, source_idx);

    while (0 != Q.length) {/*While Queue is not empty*/
        min = deleteMin(&Q); /* remove the first item in the queue*/
        /* Check to make sure the minimum value is not 'infinity'*/
        if (MAX_INDEX == min->cost) {
            destroyHeap(&Q, n_vertices + 1, NULL);
            free(min->data); free(min);
            free(vistedA);
            return NULL;
        }
        v = &g->vertexArray[min->idx]; /* get the actual vertex by index*/
        free(min->data); free(min);

        /* Mark the last taken vertex as scanned*/
        vistedA[v->node_index] = TRUE;

        /* Return the value of the node if it matches our preferences*/
        if (matchPref(g, conditions, v->node_index)) {
            distA[0] = v->node_index; /*Store destination index in 0th elem*/
            destroyHeap(&Q, n_vertices + 1, NULL);
            free(vistedA);
            return distA;
        }

        /* Get the adjacency list*/
        adjListTmp = g->adjancencyArray[v->node_index].linked_node;
        /* Loop through the Adjacency Array*/
        do {
            if (NULL == adjListTmp->data) /* Empty adjancency node*/
                break;

            u = *(edge_t*)(adjListTmp->data);
            /*Ignore the vertex if already visited*/
            if (!vistedA[u.node_to]) {
                distanceTmp = distA[v->node_index] + u.cost;

                /* We found a shorter path between nodes */
                /* Don't cheat by using integer overflow as it can be unsafe*/
                if (MAX_INDEX == distA[v->node_index] ||
                    distanceTmp < distA[u.node_to]) {
                    /* Check that we didn't overflow the uint*/
                    if (MAX_INDEX != distA[v->node_index] &&
                        distanceTmp < u.cost) {
                        debug(__LINE__, __FILE__, EXIT_FAILURE,
                              "Integer Overflow!\ndistL[%u] = %u,"
                              "length(u, v) = %u", v->node_index,
                              distA[v->node_index], u.cost);
                    }

                    /* Safely set the cost in the case of an 'infinite' 
                     * distance for the initialisation*/
                    if (MAX_INDEX == distA[v->node_index])
                        distanceTmp = u.cost;

                    /*update the distance/cost array*/
                    distA[u.node_to] = distanceTmp;

                    /* update the predecessor array*/
                    g->predecessors[u.node_to] = v->node_index;

                    /* Update the priority of u.node_to*/
                    updatePriority(&Q, u.node_to, distanceTmp);
                    upHeap(&Q, Q.idx2heap[u.node_to]);
                }
            }
            adjListTmp = adjListTmp->linked_node;
        } while (NULL != adjListTmp); /*While there is another vertex in the 
                                      adjacency array*/
    }

    /* No destination that matched the preferences found. Free stuff*/
    destroyHeap(&Q, n_vertices + 1, NULL);
    free(vistedA);
    return NULL;
}


/*Frees the graph*/
void freeGraph(Graph* g, INDEX_TYPE n_vertices) {
    INDEX_TYPE i;
    for (i = 1; i <= n_vertices; ++i) {
        if (NULL != &g->adjancencyArray[i])
            free(g->vertexArray[i].name);
        destroyList(&g->adjancencyArray[i], NULL);
    }
    free(g->adjancencyArray);
    free(g->predecessors);
    free(g->vertexArray);
}
