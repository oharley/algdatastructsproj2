/* Oliver Harley - 575246 October 2014*/

#include "linkedlist.h"

/* Returns a new node that is not linked to anything */
struct ll_node* newNode(void) {
    struct ll_node* r = (struct ll_node*)malloc_s(sizeof(struct ll_node));
    r->linked_node = NULL; r->data = NULL; r->sizeOfData = sizeof(NULL);
    return r;
}

/* Inserts a node at the head of a linked list*/
struct ll_node* insertNodeAtStart(struct ll_head *lList) {
    struct ll_node *r; /*hold the node to link to later*/

    r = newNode( ); /*creates node*/
    r->linked_node = lList->linked_node;
    lList->linked_node = r;

    ++lList->ll_length;
    return r;
}

/*Creates and returns a node at the end of the linked list lList*/
struct ll_node* insertNodeAtEnd(struct ll_head* lList) {
    struct ll_node* tmp, *r; /*hold the node to link to later*/

    tmp = lList->linked_node;
    r = newNode( ); /*creates node*/

    if (NULL == tmp)
        lList->linked_node = r;
    else {
        while (NULL != tmp->linked_node)
            tmp = tmp->linked_node;
        tmp->linked_node = r;
    }

    /* correctly initialise data as void/NULL*/
    r->data = NULL; r->sizeOfData = sizeof(NULL);
    ++lList->ll_length;
    return r;
}

/* Frees a linked list, does what you expect. If the data pointer needs
 * special freeing then pass it to this.*/
void destroyList(struct ll_head* l, void(*free_data_func(struct ll_node*n))) {
    if (NULL != l->linked_node)
        destroyListToRight(l->linked_node, (free_data_func));
}

/* Destroys all of the list to the right.
 * The free_data_func is called just before the node is free'd. Frees the data
 * pointer, this may change in later implementations.*/
void destroyListToRight(struct ll_node* n,
                        void(*free_data_func)(struct ll_node* n)) {
    if (NULL != n->linked_node) {
        destroyListToRight(n->linked_node, free_data_func);
    }
    free(n->data);
    free(n); n = NULL;
}
