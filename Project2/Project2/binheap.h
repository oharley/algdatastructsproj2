/* Oliver Harley - 575246 October 2014*/
#ifndef BINHEAP_H
#define BINHEAP_H

/*Change COST_TYPE to change the type of the priority within
 * the bin_heap_data node. It should be an unsigned integer type.*/
#define COST_TYPE unsigned int

/* Changes the maximum allowable length for the heap, should be of
 * unsigned int type, or you will have to manually change the max allowable
 * length, rather than use the USIGNED_MAXOF macro*/
#define HEAP_LEN_TYPE unsigned int

/*Returns type casted maximum value for type t. t must be unsigned integer*/
#ifndef USIGNED_MAXOF
#define USIGNED_MAXOF(t) ((t)(~(t) 0))
#endif /* !USIGNED_MAXOF */

#define GREATEST_COST USIGNED_MAXOF(COST_TYPE)
#define MAX_HEAP_LEN USIGNED_MAXOF(HEAP_LEN_TYPE)

#include "stds.h"
#include <limits.h>
#include "com_func.h"

/* Assumes Max Priority value is less than HEAP_LEN_TYPE*/
struct bin_heap_node{
    COST_TYPE cost;
    void* data;
    size_t data_size;
    HEAP_LEN_TYPE idx;
};

/*Creates a indexed min binary heap*/
typedef struct {
    struct bin_heap_node* heap;
    HEAP_LEN_TYPE length;
    HEAP_LEN_TYPE *idx2heap;
} indexd_heap;

indexd_heap makeHeap(HEAP_LEN_TYPE sizeOfHeap);
struct bin_heap_node* copyNode(struct bin_heap_node *N);
struct bin_heap_node* getLeastCost(indexd_heap *H);
void addToEnd(indexd_heap *H, struct bin_heap_node n);
void destroyHeap(indexd_heap *H, HEAP_LEN_TYPE len,
                 void(*freeDataFunc)(void*, size_t));
void downHeap(indexd_heap *H, HEAP_LEN_TYPE i);
void freeNode(struct bin_heap_node* N);
void heapSort(indexd_heap *H);
void insertCopy(indexd_heap *H, struct bin_heap_node n);
void upHeap(indexd_heap *H, HEAP_LEN_TYPE i);
struct bin_heap_node* deleteMin(indexd_heap *H);
void updatePriority(indexd_heap *H, HEAP_LEN_TYPE idx,
                    HEAP_LEN_TYPE newPriority);
void swap_nodes(indexd_heap *H, HEAP_LEN_TYPE i, HEAP_LEN_TYPE j);
#endif
