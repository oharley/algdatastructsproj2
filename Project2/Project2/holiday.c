/* Oliver Harley - 575246 October 2014*/
/* The Main file for Comp20003 Project 2 */

#define N_EXPECTED_ARGS 2

#define READ_SUCCESS 1
#define READ_FAILURE 0

#define MAXLINELEN 256
#define MAX_CONTINENT_STR_LEN 50 /*Longest region name*/
#define MAX_NAME_LEN 255 /*Guaranteed to be less than 256*/
#define EXPECTED_PLACE_FIELDS 5
#define EXPECTED_COSTS_FIELDS 3

#include "holiday.h"



/* This program finds and prints out the cheapest route according to some
 * files that specify a graph of holiday destinations, and user preferences.
 * It uses Dijkstra's algorithm to find the shortest path, and a min-priority 
 * queue based on an array based binary heap.*/
int main(int argc, char **argv) {
    /* Check program is being called correctly */
    if (N_EXPECTED_ARGS + 1 != argc) {
        printf("%s should be used as %s place_fp cost_fp\n",
               argv[0], argv[0]);
        printf("%s also reads stdin.\n", argv[0]);

        exit(EXIT_FAILURE);
    }

    /* Read input files*/
    FILE *places_file;
    FILE *edges_file;

    unsigned int n_vertices;

    Graph g;
    places_file = s_fopen(argv[1], "r");
    edges_file = s_fopen(argv[2], "r");

    /* Parse the input files*/
    g.vertexArray = parsePlaces(places_file);
    n_vertices = g.vertexArray[0].node_index;

    g.adjancencyArray = read_edges_file(edges_file, n_vertices);

    g.length = n_vertices;
    /* We are done with these files now*/
    fclose(edges_file);
    fclose(places_file);

    /* Now get User input as to where they want to go*/
    vertex_t conditions;
    getPrefs(&conditions, g.vertexArray);

    /* Find the cheapest route to their preferences, and print this info*/
    findCheapestRoute(&g, &conditions);

    /*Free any remaining malloc'd stuff*/
    freeGraph(&g, n_vertices);

    return EXIT_SUCCESS;
}

/* Checks if the vertex at index 'to' in Graph g matches the conditions vertex
 * returns 1 for true, and 0 for false. */
int nodeMatchesPreferences(Graph *g, vertex_t* conditions, INDEX_TYPE to) {
/* This function should be changed depending on how the preferences are used*/
    struct node_attributes *destAttributes, *conditionAttributes;
    destAttributes = &g->vertexArray[to].attributes;
    conditionAttributes = &conditions->attributes;

    /* We don't want to go on holiday to our own city*/
    if (conditions->node_index == to) 
        return FALSE;
    /* Check the cultural conditions, 'no' really means don't care.*/
    if (conditionAttributes->isCultural && !destAttributes->isCultural)
        return FALSE;
    if (conditionAttributes->isOutdoor && !destAttributes->isOutdoor)
        return FALSE;
    /* If a particular continent is specified, then check that the vertex we 
     * are comparing is in that continent*/
    if (Any_e != conditionAttributes->continent) {
        return (conditionAttributes->continent == destAttributes->continent);
    }

    return TRUE;
}

/* Prints out the route and costs suggested*/
void printRoute(Graph * g, INDEX_TYPE* destCosts) {
    INDEX_TYPE i, idxOfDestination, total_cost = 0;
    struct ll_head route;
    struct ll_node *tmp;
    i = idxOfDestination = *destCosts;

    /* Creates a stack(LIFO) of the vertices visted, so the top item is the 
    first visted node and the last item the last node. Uses a linked list as 
    the underlying data structure*/
    route.linked_node = newNode( );
    total_cost = destCosts[i]; /*destCosts must be an aggregate costs array*/

    /*loop while there is an earlier vertex visited*/
    while (g->predecessors[i]) {
        tmp = insertNodeAtStart(&route);
        tmp->data = malloc_s(sizeof(INDEX_TYPE));
        tmp->sizeOfData = sizeof(INDEX_TYPE);

        *(INDEX_TYPE*)tmp->data = g->predecessors[i]; /* add to stack*/
        i = g->predecessors[i]; /*Go to the predecessor of vertex i*/
    }

    tmp = route.linked_node;
    /*Print out final destination and total cost*/
    printf("%s $%u\n", g->vertexArray[idxOfDestination].name, total_cost);
    /*Print out the stack*/
    while (NULL != tmp->linked_node) {
        printf("%s ", g->vertexArray[*(INDEX_TYPE*)tmp->data].name);
        tmp = tmp->linked_node;
    }
    printf("%s\n", g->vertexArray[idxOfDestination].name);
    destroyList(&route, NULL);
}

/* Finds and prints the cheapest route based on the conditions given, and the
 * graph that we have.*/
void findCheapestRoute(Graph *g, vertex_t *conditions) {
    INDEX_TYPE idxOfDestination = UNDEF;

    /* Do the actual search*/
    INDEX_TYPE *destCosts = dijkstra(g, conditions, nodeMatchesPreferences);

    /* Check that we actually found a destination*/
    if (NULL == destCosts || FALSE == *destCosts) {
        printf("No appropriate destination found!\n");
        free(destCosts);
        return;
    }

    idxOfDestination = *destCosts;

    printRoute(g, destCosts);
    free(destCosts); 
    
}

/* This function searches for the vertex loaded.
 * Runs in O(V) time (linear search, starting from the end) */
int findStartIndex(char* name, vertex_t* vertices) {
    unsigned int i;
    char* tmp;

    str_tolower(name);

    for (i = vertices[0].node_index; i > 0; --i) {
        tmp = str_tolower(makeStrCopy(vertices[i].name));
        if (!strcmp(tmp, name)) {
            free(tmp);
            return vertices[i].node_index;
        }
        free(tmp);
    }

    /* If the name is not found within the vertex list return 0(false)*/
    return FALSE;
}

/* Fills a vertex_t with the user's inputs*/
void getPrefs(vertex_t* pref, vertex_t* vertex_list) {
    char place_name[MAX_NAME_LEN], continent_name[MAX_CONTINENT_STR_LEN];

    printf("Enter your starting point:\n");
    scan_str_s(place_name, MAX_NAME_LEN);

    /* Check that the starting position is in the graph*/
    if (!(pref->node_index = findStartIndex(place_name, vertex_list))) {
        debug(__LINE__, __FILE__, EXIT_FAILURE,
              "ERROR: %s isn't in the places file.\n", place_name);
    }

    /* New get preferences, assumes bad input/or return is 'any' or No or 0*/

    /* Get the preferred continent, if any*/
    printf("Enter the continent you prefer [Australia,Asia,NorthAmerica,"
           "SouthAmerica,Europe,Africa,Antarctica]\n");
    pref->attributes.continent = parseContinent(
        scan_str_s(continent_name, MAX_CONTINENT_STR_LEN));

    /* If nothing is specified or bad input is given, assume any continent*/
    if (Any_e == pref->attributes.continent) {
        printf("You chose 'Any'\n");
    }

    /* Get cultural preference*/

    printf("Cultural interest required? [Y/N]\n");
    pref->attributes.isCultural = parseConfirmation(lastChar( ));

    /* Get outdoor preference*/
    printf("Outdoor activities required? [Y/N]\n");
    pref->attributes.isOutdoor = parseConfirmation(lastChar( ));
    printf("\n");

    return;
}

/* Creates an edge node, and assignes the parameters to the new edge.
 * Returns the reference to the new edge.*/
edge_t* newEdge(unsigned int vertexIndex, unsigned int cost) {
    edge_t* e = (edge_t*)malloc_s(sizeof(edge_t));
    e->cost = cost; e->node_to = vertexIndex;
    return e;
}

/*Parses the edges File. See the project specs for the requirements.
 *Returns a reference to an adjancency array, in the form of an array of
 *linked lists.*/
struct ll_head* read_edges_file(FILE* fp, unsigned int n_vertices) {
    /* Create an array of Linked Lists, we will have an empty 0 vertex*/
    struct ll_head* adjancencyList = NULL;
    struct ll_node *tmp = NULL;
    unsigned int place_from, place_to, i = 0;
    int cost; /*signed - so we can check if the cost is always positive.*/

    adjancencyList = (struct ll_head*)malloc_s(
        sizeof(struct ll_head)*(1 + n_vertices));

    /* Initialise the adjacency list*/
    for (i = 0; i <= n_vertices; ++i) {
        adjancencyList[i].ll_length = 0;
        adjancencyList[i].linked_node = NULL;
    }

    /*Fill in the adjacency list and read from the file*/
    i = 0;
    while (EXPECTED_COSTS_FIELDS == fscanf(fp, "%u %u %d\n",
        &place_from, &place_to, &cost)) {
        ++i;

        /* Check that the costs are always positive*/
        if (cost < 0) {
            debug(__LINE__, __FILE__,  EXIT_FAILURE,
                  "Bad Input on line %d, negative cost (%d)!)", i, cost);
        }

        /* Put the edge into the adjacency list*/
        tmp = insertNodeAtStart(&adjancencyList[place_from]);
        tmp->data = newEdge(place_to, cost);
    }
    return adjancencyList;
}

/* Parses the places file. Expects the first line to contain an integer of
 * the records and each record to be of the form:
 * <PlaceID> <PlaceName> <PlaceContinent> <Cultural?> <Outdoors?>
 * returns reference to an array of vertices*/
vertex_t* parsePlaces(FILE* fp) {
    int n_records = 0, i = 1;
    int n_fields;

    /* Temporary holding variables for a new node*/
    int node_indx; char cultural, outdoors;
    char continent[MAX_CONTINENT_STR_LEN];
    char place_name[MAX_NAME_LEN + 1];

    vertex_t* vertex_array;

    /* Get number of records*/
    if (!fscanf(fp, "%d", &n_records)) {
        debug(__LINE__, __FILE__, EXIT_FAILURE,
              "Bad node input file, on line 1.");
    }

    /*the first vertex is kept empty, but will store the number of vertices*/
    vertex_array = malloc_s(sizeof(vertex_t)*(1 + n_records));
    vertex_array[0].node_index = n_records;

    /* Read the actual file entries*/
    while (EXPECTED_PLACE_FIELDS == (n_fields = fscanf(fp, "%d %s %s %c %c",
        &node_indx, place_name, continent, &cultural, &outdoors)) &&
        i <= n_records) {

        /*Create a new Node and copy the temporary values into the new place*/
        vertex_array[i].name = makeStrCopy(place_name);
        vertex_array[i].node_index = node_indx;
        vertex_array[i].attributes.isCultural = parseConfirmation(cultural);
        vertex_array[i].attributes.isOutdoor = parseConfirmation(outdoors);
        vertex_array[i].attributes.continent = parseContinent(continent);

        ++i;
    }

    --i;
    /* Check for bad entries within the file*/
    if (EXPECTED_PLACE_FIELDS != n_fields && EOF != n_fields) {
        debug(__LINE__, __FILE__, EXIT_FAILURE,
              "Bad input in places file on line %d",
              ++i);
    }

    /* Check that we received the right amount of entries*/
    if (i != n_records) {
        debug(__LINE__, __FILE__, EXIT_FAILURE,
              "Expected %d places, read %d\n Stopping...", n_records, i);
    }

    return vertex_array;
}

/*Parses the characters 'Y', 'y' into an int/boolean where y==1 and n==0 and
 * all other values are defaulted to No/0*/
int parseConfirmation(char confirmation) {
    switch (confirmation) {
        case 'Y':
        case 'y':
            return 1;

        case 'N':
        case 'n':
            return 0;

        default: /*By default assume "No", which is equivalent to 'either'*/
            return 0;
    }
}

/* Parses the string literals into the continent enum, ignores case*/
Continent_e parseContinent(char* str) {
    str_tolower(str);
    if (!strcmp("australia", str)) {
        return Australia_e;
    } else if (!strcmp("asia", str)) {
        return Asia_e;
    } else if (!strcmp("europe", str)) {
        return Europe_e;
    } else if (!strcmp("northamerica", str)) {
        return NorthAmerica_e;
    } else if (!strcmp("southamerica", str)) {
        return SouthAmerica_e;
    } else if (!strcmp("africa", str)) {
        return Africa_e;
    } else if (!strcmp("antartica", str)) {
        return Antartica_e;
    } else {
        return Any_e;
    }
}
