/* Oliver Harley - 575246 October 2014*/
#ifndef COM_FUNC_H
#define COM_FUNC_H

#include "stds.h"
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>

typedef struct file_list
{
    int nfiles;
    FILE **Files;
} file_list_t;

char* str_tolower(char * str);
FILE* s_fopen(const char*, const char*);
file_list_t fListOpen(const char**, const char**, int);
void * malloc_s(size_t);
void debug(int line_no, char* file, int fatal, char* fmt, ...);
void swap(void*A, void*B, size_t size);
void* calloc_s(int n_char, size_t size);
void* realloc_s(void* p, size_t size);
char* scan_str_s(char* str, unsigned int length);
char* makeStrCopy(char* str);
char lastChar(void);
#endif
