/* Oliver Harley - 575246 October 2014*/
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "stds.h"
#include "com_func.h"

/* a node that can reside within a linked list*/
struct ll_node{
    struct ll_node* linked_node;
    void* data;
    size_t sizeOfData;
};

/* The top of a linked list*/
struct ll_head{
    struct ll_node* linked_node;
    unsigned int ll_length;
};

struct ll_node* newNode(void);
struct ll_node* insertNodeAtEnd(struct ll_head* lList);
void destroyListToRight(struct ll_node* n,
                        void(*free_data_func)(struct ll_node* n));
struct ll_node* insertNodeAtStart(struct ll_head *lList);
void destroyList(struct ll_head* l, void(*free_data_func(struct ll_node*n)));
#endif
