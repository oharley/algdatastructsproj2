﻿/* Oliver Harley - 575246 October 2014*/
#ifndef HOLIDAY_H
#define HOLIDAY_H

/* the standard includes + common functions*/
#include "stds.h"
#include "com_func.h"

/*enum for regions*/
typedef enum {
    Any_e = 0, Australia_e, Asia_e, NorthAmerica_e, SouthAmerica_e,
    Europe_e, Africa_e, Antartica_e
} Continent_e;

/* What a vertex consists of*/
typedef struct vertex_t{
    unsigned int node_index;
    char* name;
    struct node_attributes{
        int isCultural;
        int isOutdoor;
        Continent_e continent;
    } attributes;
} vertex_t;

#include "dijkstra.h" /*We use dijkstra's algorithm*/
#include "binheap.h" /* Using a binary heap as our queue within dijkstra's*/
#include "linkedlist.h" /* use a linked list for a few things*/

/*Assumes the graph is undirected, and costs are symmetrical, and positive*/
typedef struct {
    HEAP_LEN_TYPE node_to;
    HEAP_LEN_TYPE cost; /* We can assume the cost is an integer,
                        as per the specs, and in dollars*/
} edge_t;

/*Function declarations*/
Continent_e parseContinent(char* str);
edge_t* newEdge(unsigned int vertexIndex, unsigned int cost);
int findStartIndex(char* name, vertex_t* vertices);
int nodeMatchesPreferences(Graph *g, vertex_t* conditions, INDEX_TYPE to);
int parseConfirmation(char confirmation);
struct ll_head* read_edges_file(FILE* fp, unsigned int n_vertices);
vertex_t* parsePlaces(FILE* fp);
void findCheapestRoute(Graph *g, vertex_t *conditions);
void getPrefs(vertex_t* pref, vertex_t* vertex_list);
void printRoute(Graph * g, INDEX_TYPE* destCosts);
void vertexFree(struct vertex_t * v);
#endif
