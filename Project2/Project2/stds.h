/* Oliver Harley - 575246 October 2014*/
#ifndef STDS_H
#define STDS_H

#define DEBUG 0

/*Debug utils for Visual Studios */
#define _CRT_SECURE_NO_WARNINGS     /* Required for Visual Studio*/
#ifdef _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#ifndef TRUE

#define TRUE (1)
#endif /* !TRUE*/

#ifndef FALSE
#define FALSE (0)
#endif /* !FALSE*/


#endif
