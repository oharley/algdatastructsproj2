/* Oliver Harley - 575246 October 2014*/
#include "com_func.h"

/* Safe fopen*/
FILE* s_fopen(const char* _Filename, const char* _Mode) {
    FILE* oFile;
    if ((oFile = fopen(_Filename, _Mode)) == NULL) {
        printf("Failed to open file:\n\t%s \"%s\".\n", _Filename, _Mode);
        exit(EXIT_FAILURE);
    }
    return oFile;
}

/* Safe realloc*/
void* realloc_s(void* p, size_t size) {
    void* pntr;
    pntr = realloc(p, size);
    if (NULL == pntr) {
        if (DEBUG) { printf("realloc failure\n"); }
        exit(EXIT_FAILURE);
    }
    return pntr;
}

/* Safe malloc*/
void* malloc_s(size_t size) {
    void* pntr;
    pntr = malloc(size);
    if (NULL == pntr) {
        if (DEBUG) { printf("malloc failure\n"); }
        exit(EXIT_FAILURE);
    }
    return pntr;
}

/* Safely creates an file_list_t and opens the files for read/write/append
    Intended to make freeing the memory later safer.
    More useful for when a large number of files .*/
file_list_t fListOpen(const char** _Filelist,
                      const char** _Modes, int nFiles) {
    int i;
    file_list_t fList;

    fList.Files = (FILE**)malloc_s(nFiles*sizeof(FILE*));
    fList.nfiles = nFiles;

    for (i = 0; i < nFiles; i++) {
        fList.Files[i] = s_fopen(_Filelist[i], _Modes[i]);
    }

    return fList;
}

/* Makes a string lowercase */
char* str_tolower(char * str) {
    int i = 0;
    while (str[i] != '\0') {
        str[i] = (char)tolower(str[i]);
        ++i;
    }
    return str;
}

/* safe calloc*/
void* calloc_s(int n_char, size_t size) {
    void* pntr;
    pntr = calloc(n_char, size);
    if (NULL == pntr) {
        if (DEBUG) { printf("calloc failure\n"); }
        exit(EXIT_FAILURE);
    }
    return pntr;
}

/* Prints the given error message, and frees the string. If fatal is non-zero
    the program will exit with a code of fatal. The line_no should be
    specified with __LINE__, and the file with __FILE__ macro.
    Really is a form of assert with a custom error message and not
    guaranteed to exit.*/
void debug(int line_no, char*file, int fatal, char* fmt, ...) {
    va_list args;
    if (DEBUG) {
        fprintf(stderr, "Error in %s:%d:\n", file, line_no);
    } else
        fputs("Error: ", stderr);

    va_start(args, fmt);

    vfprintf(stderr, fmt, args);
    va_end(args);

    if (fatal) {
        exit(fatal);
    }
}

/* Swap regions of memory A, B which have the same size*/
void swap(void*A, void*B, size_t size) {
    if (A == B)
        return;
    void* tmp = malloc_s(size);
    memcpy(tmp, B, size);
    memcpy(B, A, size);
    memcpy(A, tmp, size);
    free(tmp);
}

/*Scans a string up to length into str safely, if not exits.*/
char* scan_str_s(char* str, unsigned int length) {
    char c; unsigned int i = 0;
    char* tmp = malloc_s(sizeof(char)*length);
    while ((c = (char)getchar( )) != '\n' && c != EOF && c != '\0') {
        tmp[i++] = c;
        if (i + 1 == length) {
            free(tmp);
            debug(__LINE__, __FILE__, EXIT_FAILURE,
                  "WARNING: Input string must be less than %d in length", i);
            return NULL;
        }
    }
    tmp[i] = '\0';
    swap(tmp, str, sizeof(char)*length);
    free(tmp);
    return str;
}

/* Returns a copy of a string, allocating the right length array*/
char* makeStrCopy(char* str) {
    char* copy = (char*)malloc_s((strlen(str) + 1)*sizeof(char));
    strcpy(copy, str);
    return copy;
}

/* Returns the last character to be entered into the stream. 
 * Waits for special characters, newline, EOF, null-byte, and if there was a 
 * character from before that returns that, else returns the special char*/
char lastChar(void) {
    char d = '\0', c;
    while ((c = (char)getchar( )) != '\n' && c != EOF && c != '\0') {
        d = c;
    }
    if ('\0' == d) {
        return c;
    } else
        return d;
}

